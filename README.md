# go cricket score CLI

Download Live Cricket Score CLI Build Package  

![go Cricket Score CLI](https://codeberg.org/mskian/go-cricket-score/raw/branch/main/media/go-cricket-cli.gif)  

in Release page your find a Binary file for your system CPU Architecture
Download and install it  

- Download it from - <https://codeberg.org/mskian/go-cricket-score/releases>
- Source Code - <https://github.com/mskian/go-cricket-score>

## Usage

- Rename the binary file to `score`
- Run the CLI

```sh
score -h or --help Display Help Message
score --live or -l Display Current Live Cricket Score 
score --match <Match url> or -m <Match url> Display Current Live Cricket Score from the Cricbuzz Live Match URL
```

## Run on Andriod

- Download Termux from F-Driod
- Setup Termux - <https://github.com/mskian/my-termux-setup>
- install proot to use this CLI - DNS lookup issue when running Go app in Termux

```sh
pkg install proot
termux-chroot
./score -h

## Logout from chroot
exit
```

## LICENSE

MIT
